export enum Screen {
  Bundles = 1,
  Discount,
  Account,
  Product,
  Recipe,
  Cart,
  OrderConfirmed,
}
