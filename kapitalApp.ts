import { Navigation } from 'react-native-navigation';

import App from './screens/Home';
import { Screen } from './screens';
import theme from './theme';
import Screen2 from './screens/Discounts';
import Screen3 from './screens/Screen3';
import Product from './screens/Product';
import Recipe from './screens/Recipe';
import Cart from './screens/Cart';
import OrderConfirmed from './screens/OrderConfirmed';

Navigation.registerComponent(Screen.Bundles.toString(), () => App);
Navigation.registerComponent(Screen.Discount.toString(), () => Screen2);
Navigation.registerComponent(Screen.Account.toString(), () => Screen3);
Navigation.registerComponent(Screen.Product.toString(), () => Product);
Navigation.registerComponent(Screen.Recipe.toString(), () => Recipe);
Navigation.registerComponent(Screen.Cart.toString(), () => Cart);
Navigation.registerComponent(Screen.OrderConfirmed.toString(), () => OrderConfirmed);

const createBottomTab = (label: string, icon: any) => ({
  text: label,
  icon: icon,
  selectedIconColor: theme.colors.primary,
  selectedTextColor: theme.colors.primary,
  textColor: theme.colors.text.dark.secondary,
  iconColor: theme.colors.text.dark.secondary,
});

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: Screen.Bundles,
                    options: { topBar: { title: { text: 'K-arma' } } },
                  },
                },
              ],
              options: {
                bottomTab: createBottomTab('Bundles', require('./images/bag.png')),
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: Screen.Discount,
                  },
                },
              ],
              options: {
                bottomTab: createBottomTab('Discount', require('./images/list.png')),
              },
            },
          },
        ],
      },
    },
  });
});
