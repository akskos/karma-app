import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

import theme from '../theme';

interface Props {
  length: number;
  title: string;
}

const BundleHeader: React.SFC<Props> = props => (
  <View style={styles.listHeader}>
    <Text style={styles.listHeaderText}>{props.title}</Text>
    <TouchableOpacity>
      <Text style={styles.bundleAllButton}>See All ({props.length})</Text>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  listHeaderText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  listHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8,
    paddingHorizontal: 16,
  },
  bundleAllButton: {
    fontWeight: 'bold',
    color: theme.colors.primary,
    fontSize: 16,
  },
});

export default BundleHeader;
