import React from 'react';
import { TouchableOpacity, Text, StyleSheet, ViewProps, StyleProp, ViewStyle } from 'react-native';

import theme from '../theme';

interface Props {
  onPress(): void;
  city: string;
  style?: StyleProp<ViewStyle>;
}

const CityPicker: React.SFC<Props> = props => (
  <TouchableOpacity onPress={props.onPress} style={props.style}>
    <Text style={styles.citySelectorText}>
      Offers in <Text style={styles.citySelectorTextHighlight}>{props.city}</Text>{' '}
      <Text style={styles.carot}>&#9660;</Text>
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  citySelectorText: {
    fontSize: 28,
    marginBottom: 24,
    fontWeight: 'bold',
  },
  citySelectorTextHighlight: {
    color: theme.colors.primary,
  },
  carot: {
    color: '#ccc',
    fontSize: 18,
  },
});

export default CityPicker;
