# Karma

Save wasted food from stores & and save money doing it! 🌭💰

![alt text](screenshot.png)

## Technologies

- React Native 🚀
- TypeScript 💙

## Related Projects

The app uses [Karma backend](https://bitbucket.org/akskos/kapital) which uses [K Group API](https://kesko.portal.azure-api.net/)

## How to run?

⚠️ This is iOS app so OSX is needed!

1. Install dependencies with `npm i`
2. Open Xcode project file with `open ios/kapitalApp.xcodeproj`
3. Press `cmd + r` to run the project on iOS simulator
