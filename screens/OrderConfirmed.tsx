import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Animated,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import theme from '../theme';
import { Navigation } from 'react-native-navigation';

type Props = {
  componentId: string;
  resetCart: (() => void) | undefined;
};

class OrderConfirmed extends React.Component<Props> {
  barAnimation = new Animated.Value(-Dimensions.get('window').width / 2);
  titleAnimation = new Animated.Value(0);
  bodyAnimation = new Animated.Value(0);
  stat1Animation = new Animated.Value(0);
  stat2Animation = new Animated.Value(0);
  stat3Animation = new Animated.Value(0);
  barAppearAnim = new Animated.Value(0);
  buttonAnimation = new Animated.Value(0);

  componentDidMount() {
    setTimeout(() => {
      Animated.timing(this.titleAnimation, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400,
      }).start();
    }, 250);
    setTimeout(() => {
      Animated.timing(this.stat1Animation, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400,
      }).start();
    }, 500);
    setTimeout(() => {
      Animated.timing(this.stat2Animation, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400,
      }).start();
    }, 750);
    setTimeout(() => {
      Animated.timing(this.stat3Animation, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400,
      }).start();
    }, 1000);
    setTimeout(() => {
      Animated.timing(this.barAppearAnim, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400,
      }).start();
    }, 1250);
    setTimeout(() => {
      Animated.timing(this.barAnimation, {
        toValue: -Dimensions.get('window').width / 2 + 100,
        useNativeDriver: true,
        duration: 500,
      }).start();
    }, 1750);
    setTimeout(() => {
      Animated.timing(this.bodyAnimation, {
        toValue: 1,
        useNativeDriver: true,
        duration: 500,
      }).start();
    }, 2500);
    setTimeout(() => {
      Animated.timing(this.buttonAnimation, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400,
      }).start();
    }, 3000);
  }

  closeModal = () => {
    this.props.resetCart && this.props.resetCart();
    Navigation.dismissAllModals();
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.content} contentInset={{ bottom: 80 }}>
          <Animated.View
            style={{
              opacity: this.titleAnimation,
              transform: [
                {
                  translateY: this.titleAnimation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-40, 0],
                  }),
                },
              ],
            }}
          >
            <Text style={styles.title}>Your order is complete!</Text>
            <Text style={styles.subtitle}>ID: 12796424680</Text>
          </Animated.View>
          <Animated.View style={[styles.newStats]}>
            <Animated.View style={[styles.newStat, { opacity: this.stat1Animation }]}>
              <Text style={styles.newStatValue}>224€</Text>
              <Text style={styles.newStatLabel}>Euros saved</Text>
            </Animated.View>
            <Animated.View style={[styles.newStat, { opacity: this.stat2Animation }]}>
              <Text style={styles.newStatValue}>Lvl. 3</Text>
              <Text style={styles.newStatLabel}>K-arma</Text>
            </Animated.View>
            <Animated.View style={[styles.newStat, { opacity: this.stat3Animation }]}>
              <Text style={styles.newStatValue}>300</Text>
              <Text style={styles.newStatLabel}>points to lvl. 4</Text>
            </Animated.View>
          </Animated.View>
          <Animated.View style={[styles.progressBar, { opacity: this.barAppearAnim }]}>
            <Animated.View
              style={[styles.progressBarInner, { transform: [{ translateX: this.barAnimation }] }]}
            />
          </Animated.View>
          <Animated.View style={[styles.body, { opacity: this.bodyAnimation }]}>
            <Text style={styles.bodyTitle}>Benefits at Level 3</Text>
            <Text style={styles.bodyBullet}>- Every 6th purchase gets a 50% discount</Text>
            <Text style={styles.bodyBullet}>- Monthly Free coupon for a coffee at any K-mart</Text>
            <Text style={styles.bodyBullet}>- 1 half price delivery</Text>
            <Text style={styles.bodyTitle}>What are K-arma points?</Text>
            <Text style={styles.bodyPara}>
              Every time you buy something through the K-arma app, you are helping reduce wasted
              resources and lower your carbon footprint.
            </Text>
            <Text style={styles.bodyPara}>
              We score products based on the resources needed to produce them, gathered from
              independent research by companies like Sitra and WWF.
            </Text>
            <Text style={styles.bodyPara}>
              Every time you buy something through the K-arma app, you are helping reduce wasted
              resources and lower your carbon footprint.
            </Text>
            <Text style={styles.bodyPara}>
              We score products based on the resources needed to produce them, gathered from
              independent research by companies like Sitra and WWF.
            </Text>
          </Animated.View>
        </ScrollView>
        <Animated.View
          style={[
            styles.buyButton,
            {
              transform: [
                {
                  translateY: this.buttonAnimation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [70, 0],
                  }),
                },
              ],
            },
          ]}
        >
          <TouchableOpacity onPress={this.closeModal}>
            <Text style={styles.buyButtonText}>Okay</Text>
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    paddingTop: 20,
  },
  container: {
    flex: 1,
  },
  newStats: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    marginTop: 16,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 8,
  },
  subtitle: {
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 48,
  },
  newStatValue: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
  },
  newStatLabel: {
    textAlign: 'center',
    fontSize: 18,
    color: '#838383',
  },
  body: {
    paddingHorizontal: 16,
  },
  bodyTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 24,
    marginBottom: 8,
  },
  bodyBullet: {
    fontSize: 16,
  },
  bodyPara: {
    fontSize: 16,
    marginBottom: 8,
  },
  progressBar: {
    height: 20,
    backgroundColor: '#E5E5E5',
    flex: 1,
    marginHorizontal: 16,
    borderRadius: 10,
    marginTop: 24,
    overflow: 'hidden',
  },
  progressBarInner: {
    flex: 1,
    backgroundColor: theme.colors.primary,
    borderRadius: 10,
  },
  buyButton: {
    backgroundColor: theme.colors.primary,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  buyButtonText: {
    color: '#ffffff',
    fontSize: 18,
    textAlign: 'center',
    padding: 16,
  },
});

export default OrderConfirmed;
