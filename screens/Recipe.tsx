import React from 'react';
import { View, WebView, StyleSheet, TouchableOpacity, Text } from 'react-native';
import theme from '../theme';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import { Screen } from '../screens';
import { ProductType } from '../types';

export interface RecipeProps {
  url: string;
  id: string;
}

interface State {
  ingredients: ProductType[];
}

class Recipe extends React.Component<RecipeProps, State> {
  state: Readonly<State> = { ingredients: [] };

  async componentDidMount() {
    const res = await axios.get<{ results: ProductType[] }>(
      'https://kmarket-junction-thing.herokuapp.com/recipe/' + this.props.id + '/ingredients',
    );
    console.log('perkele', res.data.results);
    this.setState({ ingredients: res.data.results });
  }

  goToCheckout = () => {
    Navigation.showModal({
      component: {
        name: Screen.Cart,
        passProps: { products: this.state.ingredients },
      },
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <WebView
          source={{
            uri: this.props.url,
          }}
          style={styles.webView}
        />
        {!!this.state.ingredients.length && (
          <View style={styles.button}>
            <TouchableOpacity onPress={this.goToCheckout}>
              <Text style={styles.buttonText}>Add to cart</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  webView: { flex: 1 },
  container: { flex: 1 },
  button: {
    backgroundColor: theme.colors.primary,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 18,
    textAlign: 'center',
    padding: 16,
  },
});

export default Recipe;
