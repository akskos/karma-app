import React from 'react';
import {
  View,
  Text,
  FlatList,
  ListRenderItem,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import { ProductType } from '../types';
import theme from '../theme';
import { Navigation } from 'react-native-navigation';
import { Screen } from '../screens';

interface Props {
  products: ProductType[];
  componentId: string;
  resetCart(): void;
}

interface State {
  deliveryMethod?: 'pickup' | 'delivery';
}

class Cart extends React.Component<Props, State> {
  state: Readonly<State> = {};

  renderItem: ListRenderItem<ProductType> = ({ item }) => {
    const imageUrl = item.pictureUrls[0];
    return (
      <View>
        <View key={item.ean} style={styles.product}>
          {!!imageUrl && (
            <Image
              source={{
                uri: imageUrl.original,
              }}
              style={styles.productImage}
            />
          )}
          <View style={styles.productContent}>
            <Text style={styles.productName} numberOfLines={1}>
              {item.marketingName.finnish}
            </Text>
            <View style={styles.productInfo}>
              <Text style={styles.productPrice}>{item.price} €</Text>
              <Text style={styles.productDiscountPercent}>
                -{parseInt((item.discountPercentage * 100).toString())}%
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  closeCart = () => {
    Navigation.dismissModal(this.props.componentId);
  };
  renderItemSeparator = () => <View style={styles.itemSeparator} />;

  selectPickup = () => {
    this.setState({ deliveryMethod: 'pickup' });
  };

  selectDelivery = () => {
    this.setState({ deliveryMethod: 'delivery' });
  };

  buy = () => {
    Navigation.showModal({
      component: {
        name: Screen.OrderConfirmed,
        passProps: { resetCart: this.props.resetCart },
      },
    });
  };

  renderFooter = () => (
    <>
      <View style={styles.buttons}>
        <View
          style={[styles.button, this.state.deliveryMethod === 'pickup' && styles.buttonIsActive]}
        >
          <TouchableOpacity style={styles.buttonTouchable} onPress={this.selectPickup}>
            <Text
              style={[
                styles.buttonText,
                this.state.deliveryMethod === 'pickup' && styles.buttonTextActive,
              ]}
            >
              Pickup +5€
            </Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.or}>or</Text>
        <View
          style={[styles.button, this.state.deliveryMethod === 'delivery' && styles.buttonIsActive]}
        >
          <TouchableOpacity style={styles.buttonTouchable} onPress={this.selectDelivery}>
            <Text
              style={[
                styles.buttonText,
                this.state.deliveryMethod === 'delivery' && styles.buttonTextActive,
              ]}
            >
              Delivery +10€
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.priceStuff}>
        <Text style={styles.totalPrice}>Total Price</Text>
        <Text style={styles.totalPriceAmount}>
          {this.props.products.reduce((acc, product) => product.price + acc, 0) +
            (!!this.state.deliveryMethod
              ? this.state.deliveryMethod === 'pickup'
                ? 5
                : 10
              : 0)}{' '}
          €
        </Text>
      </View>
    </>
  );

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={this.closeCart}>
            <Image source={require('../images/close.png')} style={styles.closeIcon} />
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>Your cart</Text>
        <FlatList
          data={this.props.products}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.renderItemSeparator}
          style={styles.products}
          ListFooterComponent={this.renderFooter}
          keyExtractor={item => item.ean}
          extraData={this.state}
          contentInset={{ bottom: 70 }}
        />
        {!!this.state.deliveryMethod && (
          <View style={styles.buyButton}>
            <TouchableOpacity onPress={this.buy}>
              <Text style={styles.buyButtonText}>Confirm payment</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topBar: {
    paddingTop: 20,
  },
  closeIcon: {
    margin: 16,
  },
  titleWrapper: {
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#E9E9E9',
    paddingVertical: 8,
  },
  picker: {
    paddingTop: 16,
  },
  products: {
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 28,
    marginLeft: 16,
    marginBottom: 24,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  product: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  productImage: {
    width: 60,
    height: 50,
    marginRight: 8,
  },
  productContent: {
    justifyContent: 'center',
    flex: 1,
    marginRight: 8,
  },
  productAddToCart: {
    borderWidth: 1,
    borderColor: '#979797',
    borderRadius: 5,
    marginLeft: 'auto',
  },
  productName: {
    fontSize: 14,
  },
  productInfo: {
    flexDirection: 'row',
  },
  productPrice: {
    marginRight: 8,
    fontSize: 18,
  },
  productDiscountPercent: {
    color: '#D90000',
    fontSize: 18,
  },
  itemSeparator: {
    height: 10,
  },
  productLabel: {
    padding: 8,
  },
  cartButton: {
    borderRadius: 30,
    backgroundColor: theme.colors.primary,
    position: 'absolute',
    right: 16,
    bottom: 16,
  },
  cartButtonTouchable: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartButtonCount: {
    position: 'absolute',
    left: -4,
    top: -4,
    borderWidth: 1,
    borderColor: theme.colors.primary,
    width: 26,
    height: 26,
    borderRadius: 13,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartButtonCountText: {
    color: theme.colors.primary,
    fontSize: 16,
  },
  totalPriceAmount: {
    fontSize: 36,
  },
  totalPrice: {
    fontSize: 20,
    marginBottom: 8,
  },
  priceStuff: {
    paddingVertical: 16,
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 32,
  },
  button: {
    borderWidth: 2,
    borderColor: '#d3d3d3',
    borderRadius: 5,
  },
  buttonIsActive: {
    backgroundColor: theme.colors.primary,
    borderColor: theme.colors.primary,
  },
  buttonTouchable: {
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  or: {
    marginHorizontal: 16,
  },
  buttonText: {
    fontSize: 16,
  },
  buttonTextActive: {
    color: '#ffffff',
  },
  buyButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: theme.colors.primary,
  },
  buyButtonText: {
    color: '#ffffff',
    fontSize: 18,
    textAlign: 'center',
    padding: 16,
  },
});

export default Cart;
