import React from 'react';
import { View, WebView, StyleSheet } from 'react-native';

export interface ProductProps {
  productUrlPath: string;
}

class Product extends React.Component<ProductProps> {
  render() {
    return (
      <View style={styles.container}>
        <WebView
          source={{
            uri: `https://www.k-ruoka.fi/kauppa/tuote/${this.props.productUrlPath}`,
          }}
          style={styles.webView}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  webView: { flex: 1 },
  container: { flex: 1 },
});

export default Product;
