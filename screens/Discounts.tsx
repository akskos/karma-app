import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActionSheetIOS,
  SectionList,
  SectionListData,
  ListRenderItem,
  Image,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';

import CityPicker from '../components/CityPicker';
import { Navigation } from 'react-native-navigation';
import { Screen } from '../screens';
import { ProductProps } from './Product';
import theme from '../theme';
import { ProductType } from '../types';

interface Section {
  title: string;
  data: ProductType[];
}

interface State {
  selectedCity: string;
  sections: Section[];
  cartProducts: ProductType[];
}

type Props = { componentId: string };
export default class Screen2 extends Component<Props, State> {
  state: Readonly<State> = { selectedCity: 'Kallio', sections: [], cartProducts: [] };

  async componentDidMount() {
    try {
      const res = await axios.get<{ results: ProductType[] }>(
        'https://kmarket-junction-thing.herokuapp.com/products-on-sale',
      );

      const lolSections = res.data.results
        .filter(product => !!product.category)
        .reduce((acc, item) => {
          const foundIndex = acc.findIndex(lolItem => lolItem.title === item.category.finnish);
          if (foundIndex > -1) {
            acc[foundIndex].data.push(item);
          } else {
            acc.push({ title: item.category.finnish, data: [item] });
          }
          return acc;
        }, []);
      this.setState({ sections: lolSections });
    } catch (error) {
      console.log(error);
    }
  }

  pickCity = () => {
    const options = ['Kallio', 'Sörnäinen', 'Punavuori', 'Cancel'];
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex: options.length - 1,
      },
      buttonIndex => {
        if (options.length - 1 !== buttonIndex) {
          this.setState({ selectedCity: options[buttonIndex] });
        }
      },
    );
  };

  renderTitle = (info: { section: SectionListData<ProductType> }) => (
    <View style={styles.titleWrapper}>
      <Text style={styles.title}>{info.section.title}</Text>
    </View>
  );

  openProductView = (product: ProductType) => {
    Navigation.push<ProductProps>(this.props.componentId, {
      component: {
        name: Screen.Product,
        passProps: { productUrlPath: product.urlSlug },
      },
    });
  };

  goToCheckout = () => {
    Navigation.showModal({
      component: {
        name: Screen.Cart,
        passProps: { products: this.state.cartProducts, resetCart: this.resetCart },
      },
    });
  };

  resetCart = () => {
    this.setState({ cartProducts: [] });
  };

  addProductToCart = (product: ProductType) => {
    this.setState({ cartProducts: [...this.state.cartProducts, product] });
  };

  renderDiscount: ListRenderItem<ProductType> = ({ item }) => {
    const imageUrl = item.pictureUrls[0];
    const isInCart = this.state.cartProducts.some(cartProduct => item.ean === cartProduct.ean);
    return (
      <TouchableOpacity onPress={() => this.openProductView(item)}>
        <View key={item.ean} style={styles.product}>
          {!!imageUrl && (
            <Image
              source={{
                uri: imageUrl.original,
              }}
              style={styles.productImage}
            />
          )}
          <View style={styles.productContent}>
            <Text style={styles.productName} numberOfLines={1}>
              {item.marketingName.finnish}
            </Text>
            <View style={styles.productInfo}>
              <Text style={styles.productPrice}>{item.price} €</Text>
              <Text style={styles.productDiscountPercent}>
                -{parseInt((item.discountPercentage * 100).toString())}%
              </Text>
            </View>
          </View>
          <View style={styles.productAddToCart}>
            <TouchableOpacity onPress={() => this.addProductToCart(item)} disabled={isInCart}>
              <Text style={styles.productLabel}>{isInCart ? 'Added to cart' : '+ cart'}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  renderItemSeparator = () => <View style={styles.itemSeparator} />;

  renderHeader = () => (
    <CityPicker onPress={this.pickCity} city={this.state.selectedCity} style={styles.picker} />
  );

  render() {
    return (
      <View style={styles.container}>
        <SectionList
          ListHeaderComponent={this.renderHeader}
          renderItem={this.renderDiscount}
          renderSectionHeader={this.renderTitle}
          sections={this.state.sections}
          keyExtractor={(item, index) => item.ean}
          ItemSeparatorComponent={this.renderItemSeparator}
          style={styles.products}
        />
        <View style={styles.cartButton}>
          <TouchableOpacity style={styles.cartButtonTouchable} onPress={this.goToCheckout}>
            <Image source={require('../images/cart.png')} />
            <View style={styles.cartButtonCount}>
              <Text style={styles.cartButtonCountText}>{this.state.cartProducts.length}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleWrapper: {
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#E9E9E9',
    paddingVertical: 8,
  },
  picker: {
    paddingTop: 16,
  },
  products: {
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  product: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  productImage: {
    width: 60,
    height: 50,
    marginRight: 8,
  },
  productContent: {
    justifyContent: 'center',
    flex: 1,
    marginRight: 8,
  },
  productAddToCart: {
    borderWidth: 1,
    borderColor: '#979797',
    borderRadius: 5,
    marginLeft: 'auto',
  },
  productName: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  productInfo: {
    flexDirection: 'row',
  },
  productPrice: {
    marginRight: 8,
    fontSize: 18,
  },
  productDiscountPercent: {
    color: '#D90000',
    fontSize: 18,
  },
  itemSeparator: {
    height: 10,
  },
  productLabel: {
    padding: 8,
  },
  cartButton: {
    borderRadius: 30,
    backgroundColor: theme.colors.primary,
    position: 'absolute',
    right: 16,
    bottom: 16,
  },
  cartButtonTouchable: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartButtonCount: {
    position: 'absolute',
    left: -4,
    top: -4,
    borderWidth: 1,
    borderColor: theme.colors.primary,
    width: 26,
    height: 26,
    borderRadius: 13,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartButtonCountText: {
    color: theme.colors.primary,
    fontSize: 16,
  },
});
