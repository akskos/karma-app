import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ListRenderItem,
  TouchableOpacity,
  Image,
  Dimensions,
  ActionSheetIOS,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';

import theme from '../theme';
import BundleHeader from '../components/BundleHeader';
import CityPicker from '../components/CityPicker';
import { Navigation } from 'react-native-navigation';
import { RecipeProps } from './Recipe';
import { Screen } from '../screens';
import { ProductType } from '../types';

interface PictureUrl {
  Normal: string;
}

interface RecipeBundle {
  Name: string;
  PictureUrls: PictureUrl[];
  Url: string;
  Id: string;

  moneySaving: number;
  co2Saving: number;
}

interface ThemeBundle {
  name: string;
  productCount: number;
  moneySaving: number;
  co2Saving: number;
  results: ProductType[];
}

interface State {
  selectedCity: string;
  recipeBundles: RecipeBundle[];
  themedBundles: ThemeBundle[];
}

const randomIntBetween = (from: number, to: number) => Math.floor(Math.random() * to) + from;

// https://source.unsplash.com/320x240/?food

const hardCodedThemeBundlePictures = [
  require('../images/food1.jpeg'),
  require('../images/food2.jpeg'),
  require('../images/food3.jpeg'),
  require('../images/food4.jpeg'),
  require('../images/food5.jpeg'),
  require('../images/food6.jpeg'),
];

type Props = { componentId: string };
export default class App extends Component<Props, State> {
  state: Readonly<State> = { selectedCity: 'Kallio', recipeBundles: [], themedBundles: [] };

  componentDidMount() {
    this.fetchRecipes();
    this.fetchThemeBundles();
  }

  fetchRecipes = async () => {
    try {
      const res = await axios.get<{ results: RecipeBundle[] }>(
        'https://kmarket-junction-thing.herokuapp.com/recipes-on-sale',
      );

      this.setState({ recipeBundles: res.data.results });
    } catch (error) {
      console.log(error);
    }
  };

  fetchThemeBundles = async () => {
    try {
      const res = await axios.get<ThemeBundle[]>(
        'https://kmarket-junction-thing.herokuapp.com/themed-bundles',
      );

      this.setState({ themedBundles: res.data });
    } catch (error) {
      console.log(error);
    }
  };

  openRecipeScreen = (recipe: RecipeBundle) => {
    Navigation.push<RecipeProps>(this.props.componentId, {
      component: {
        name: Screen.Recipe,
        passProps: { url: recipe.Url, id: recipe.Id },
      },
    });
  };

  renderBundle: ListRenderItem<RecipeBundle> = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.openRecipeScreen(item)}>
        <View style={styles.bundle}>
          <View style={styles.bundleImageWrapper}>
            <Image
              source={{
                uri: item.PictureUrls[0].Normal,
              }}
              style={styles.bundleImage}
            />
          </View>
          <View style={styles.bundleUpperPart}>
            <Text style={styles.bundleName} numberOfLines={1}>
              {item.Name}
            </Text>
          </View>
          <View style={styles.bundleLowPart}>
            <Text style={styles.bundleMoneySaving}>{randomIntBetween(4, 12)} € savings</Text>
            <Text style={styles.bundleco2Saving}>{randomIntBetween(40, 200)} K-arma</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  openCart = (themeBundle: ThemeBundle) => {
    Navigation.showModal({
      component: {
        name: Screen.Cart,
        passProps: { products: themeBundle.results },
      },
    });
  };

  renderThemeBundle: ListRenderItem<ThemeBundle> = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => this.openCart(item)}>
        <View style={styles.themeBundle}>
          <View style={styles.themedBundleImageWrapper}>
            <Image source={hardCodedThemeBundlePictures[index]} style={styles.themedBundleImage} />
            <LinearGradient colors={['transparent', '#000000']} style={styles.linearGradient}>
              <Text style={styles.themeBundleName}>{item.name}</Text>
              <Text style={styles.themeBundleProductCount}>{item.results.length} products</Text>
            </LinearGradient>
          </View>
          <View style={styles.themeBundleContent}>
            <Text>{randomIntBetween(4, 12)} € savings</Text>
            <Text>{randomIntBetween(40, 200)} K-arma</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  pickCity = () => {
    const options = ['Kallio', 'Sörnäinen', 'Punavuori', 'Cancel'];
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex: options.length - 1,
      },
      buttonIndex => {
        if (options.length - 1 !== buttonIndex) {
          this.setState({ selectedCity: options[buttonIndex] });
        }
      },
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <CityPicker city={this.state.selectedCity} onPress={this.pickCity} style={styles.picker} />
        <BundleHeader title="Recipe Bundles" length={this.state.recipeBundles.length} />
        <FlatList
          data={this.state.recipeBundles}
          renderItem={this.renderBundle}
          horizontal
          style={styles.bundles}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.Id}
        />
        <BundleHeader title="Themed bundles" length={this.state.themedBundles.length} />
        <FlatList
          data={this.state.themedBundles}
          renderItem={this.renderThemeBundle}
          horizontal
          style={styles.themedBundles}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const iosStatusBarHeight = 20;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
  },
  picker: {
    marginLeft: 16,
  },
  citySelectorText: {
    fontSize: 28,
    marginBottom: 24,
    marginLeft: 16,
  },
  citySelectorTextHighlight: {
    color: theme.colors.primary,
  },
  listHeaderText: {
    fontSize: 20,
  },
  listHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8,
    paddingHorizontal: 16,
  },
  bundleAllButton: {
    color: theme.colors.primary,
    fontSize: 16,
  },
  bundles: {
    paddingLeft: 16,
  },
  bundle: {
    backgroundColor: '#ffffff',
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    width: Dimensions.get('window').width / 2,
    marginRight: 16,
    borderRadius: 5,
  },
  bundleUpperPart: {
    padding: 8,
  },
  bundleName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  bundleLowPart: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
  },
  bundleMoneySaving: {
    fontSize: 14,
  },
  bundleco2Saving: {
    fontSize: 14,
  },
  bundleImageWrapper: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
  },
  bundleImage: {
    height: 100,
  },
  themedBundles: {
    paddingLeft: 16,
  },
  themeBundle: {
    backgroundColor: '#ffffff',
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    width: Dimensions.get('window').width / 2,
    marginRight: 16,
    borderRadius: 5,
  },
  themedBundleImageWrapper: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
  },
  themedBundleImage: {
    height: 100,
  },
  themeBundleContent: {
    padding: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  linearGradient: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 8,
  },
  themeBundleName: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  themeBundleProductCount: {
    color: '#ffffff',
    fontSize: 14,
  },
});
