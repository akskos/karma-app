interface Picture {
  original: string;
}

export interface ProductType {
  marketingName: { finnish: string };
  ean: string;
  category?: { finnish: string };
  pictureUrls: Picture[];
  urlSlug: string;

  price: number;
  discountPercentage: number;
}
