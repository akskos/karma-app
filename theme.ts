export default {
  colors: {
    primary: '#ff6900',
    text: { dark: { primary: '#000000', secondary: '#95a5a6' } },
  },
};
